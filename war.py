# python 3.4

import queue
from random import shuffle
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-q", "--quiet", help="Only displays the final winner", action="store_true")
args = parser.parse_args()

def myPrint(myStr):
    if args.quiet:
       return
    print(myStr)

def giveCards(winnerQueue, winnerList):
    shuffle(winnerList)
    for card in winnerList:
        winnerQueue.put(card)
    return winnerQueue

def battle(playerA, playerB, battleGround):
    card_A = playerA.get()
    card_B = playerB.get()
    battleGround.extend([card_A, card_B])
    if card_A > card_B:
        myPrint("Player A wins with a %s%s against %s%s" % (numToLabel[card_A[0]], card_A[1], numToLabel[card_B[0]], card_B[1]))
        playerA = giveCards(playerA, battleGround)
    if card_A < card_B:
        myPrint("Player B wins with a %s%s against %s%s" % (numToLabel[card_A[0]], card_A[1], numToLabel[card_B[0]], card_B[1]))
        playerB = giveCards(playerB, battleGround)
    if card_A == card_B:
        myPrint("WAR!!!")
        for x in range(0,3):
            battleGround.extend([playerA.get(), playerB.get()])
            playerA, playerB = battle(playerA, playerB, battleGround)
    return(playerA, playerB)

playerA = queue.Queue()
playerB = queue.Queue()
suits = ["D","C","H","S"]
numToLabel = {1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'10', 11:'J', 12:'Q', 13:'K', 14:'A'}
deck = []

for suit in suits:
    for num in range(1,15):
        card = [num, suit]
        deck.append(card)

shuffle(deck)
playerA_tempList = deck[::2]
playerB_tempList = deck[1::2]
for card in playerA_tempList:
    playerA.put(card)
for card in playerB_tempList:
    playerB.put(card)

roundNum = 1
while(playerA.qsize() > 0 and playerB.qsize() > 0):
    battleGround = []
    playerA, playerB = battle(playerA, playerB, battleGround)
    myPrint("\tRound %s: Player A now has %s cards and player B has %s cards" % (roundNum, playerA.qsize(), playerB.qsize()))
    roundNum += 1

if playerA.qsize() > playerB.qsize():
    print("PLAYER A WINS!!! in %s rounds" % roundNum)
else:
    print("PLAYER B WINS!!! in %s rounds" % roundNum)

