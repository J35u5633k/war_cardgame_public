### What is this repository for? ###

Python implementation of the card game "war". A script I built with my kids that plays both players of the game.

https://en.wikipedia.org/wiki/War_(card_game)

### How do I get set up? ###

This was written and tested with Python 3.4, but should work with a default install of any python3 version after 3.2.  Not tested with 2.x.

The python script should run on any OS, but the bash script will only work on a unix/linux OS.

To run one game, run with the -h first:

```
#!bash
python3 war.py -h
```

To run multiple games and track the running average:

```
#!bash
bash war_runningAvg.sh
```

### Who do I talk to? ###

Send me a private message from your gitlab.com inbox